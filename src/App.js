import React, { Component } from 'react';
import { Link, Route, Switch } from 'react-router-dom';

import './App.css';

import Home from './containers/Home/index';

import AddResponse from './containers/Responses/Add';
import BrowseResponse from './containers/Responses/Browse';
import Response from './containers/Response';

import AddMock from './containers/Mocks/Add';
import BrowseMocks from './containers/Mocks/Browse';
import Mock from './containers/Mock';

import NotFoundPage from './components/NotFoundPage';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to json mock!</h1>
        </header>

        <nav className="App-nav">
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/responses/add">Create new response</Link></li>
            <li><Link to="/responses/browse" >Browse responses</Link></li>
            <li><Link to="/mocks/add" >Create new mock</Link></li>
            <li><Link to="/mocks/browse" >Browse mocks</Link></li>
          </ul>
        </nav>

        <div className="Wrapper">
          <Switch>
            <Route exact path="/" component={Home} />

            <Route exact path="/responses/add" component={AddResponse}/>
            <Route exact path="/responses/browse" component={BrowseResponse}/>
            <Route path="/response/:id" component={Response}/>

            <Route exact path="/mocks/add" component={AddMock}/>
            <Route exact path="/mocks/browse" component={BrowseMocks}/>
            <Route path="/mock/:id" component={Mock}/>

            <Route path="*" component={NotFoundPage}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
