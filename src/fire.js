import firebase from 'firebase';

 // Initialize Firebase
const config = {
	apiKey: "AIzaSyCWxBvx1MvUhCwan8dEcvMSjntWW_0jbYk",
	authDomain: "jsonmock-cb2ed.firebaseapp.com",
	databaseURL: "https://jsonmock-cb2ed.firebaseio.com",
	projectId: "jsonmock-cb2ed",
	storageBucket: "jsonmock-cb2ed.appspot.com",
	messagingSenderId: "437674086167"
};

const fire = firebase.initializeApp(config);
const dbRef = fire.database();
const responsesDbRef = dbRef.ref().child('responses');
const mocksDbRef = dbRef.ref().child('mocks');


export default fire;
export { dbRef, responsesDbRef, mocksDbRef };