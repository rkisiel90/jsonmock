import shortid from 'shortid';


/**
 * Takes an array of items and enahce it with unique key
 * @param {string} data Array of single items to enhance
 * @returns {array} Array of enhanced objects
 */
export function enhanceResponsesWithUnique(data) {
  return data.map(item => enhanceItemWithUnique(item));
}

/**
 * Takes an item and returns enhenced object - with unique key
 * @param {string} data Array of single items to enhance
 * @returns {array} Array of enhanced objects
 */
export function enhanceItemWithUnique(item) {
  return {
    id: item,
    unique: shortid.generate()
  }
}