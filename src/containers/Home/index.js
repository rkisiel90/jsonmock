import React, { Component } from 'react';

import MocksList from '../Mocks/List';
import ResponsesList from '../Responses/List';


class Home extends Component {
  render() {
    return (
      <div className="Home">
        <MocksList />
        <ResponsesList />
      </div>
    );
  }
}

export default Home;
