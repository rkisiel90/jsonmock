import React, { Component } from 'react';
import ResponsesList from '../List';


class BrowseResponse extends Component {
  constructor(){
    super();

    this.state = {
    };
  }


  render() {
    return (
      <div className="BrowseResponseWrapper">
        <h2>Browse responses: </h2>

        <ResponsesList />
      </div>
    );
  }
}

export default BrowseResponse;
