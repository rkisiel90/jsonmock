import React, { Component } from 'react';
import ReactJson from 'react-json-view';

import { responsesDbRef } from '../../../fire';



class AddResponse extends Component {
  constructor(){

    super();
    this.state = {
      id: '',
      body: '',
      bodyParsed: {},
      description: ''
    };

    this.handleIdChange = this.handleIdChange.bind(this);
    this.handleBodyChange = this.handleBodyChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleJsonEdit = this.handleJsonEdit.bind(this);
    this.setDefaultState = this.setDefaultState.bind(this);

    this.newResponse = {};
  }

  componentDidMount() {
    this.newResponse = responsesDbRef.push();

    this.setState({
      id: this.newResponse.key
    });
  }

  handleSubmit(e) {

    e.preventDefault();

    // if we have parsed JSON
    if(Object.keys(this.state.bodyParsed).length) {

      // should we use auto-generated reference or should we create our own?
      const newEntryRef = (this.newResponse.key === this.state.id) ? this.newResponse : responsesDbRef.child(this.state.id);

      newEntryRef.set({
        body: this.state.bodyParsed,
        description: this.state.description
      });

      this.setDefaultState();
    } else {
      alert('You must provide body as a JSON!');
    }
  }

  handleJsonEdit(event) {
    this.handleBodyChange({
      target: {
        value: JSON.stringify(event.updated_src)
      }
    })
  }

  setDefaultState() {
    this.newResponse = responsesDbRef.push();

    this.setState({
      id: this.newResponse.key,
      body: '',
      bodyParsed: {},
      description: ''
    });
  }

  handleIdChange(event) {

    this.setState({id: event.target.value});
  }

  handleBodyChange(event) {

    try {
      this.setState({bodyParsed: JSON.parse(event.target.value)})
    } catch(e) {
      this.setState({bodyParsed: {}})
    }

    this.setState({body: event.target.value});
  }

  handleDescriptionChange(event) {

    this.setState({description: event.target.value});
  }

  render() {
    return (
      <div className="AddResponseWrapper">
        <h2> Add new response: </h2>


          <form onSubmit={this.handleSubmit}>
            <div className="form-block">
              <label>Id:</label>
              <input type="text" onChange={this.handleIdChange} value={this.state.id} />
            </div>
            <div className="form-block">
              <label>Body (JSON):</label>
              <textarea name="response_body" value={this.state.body} onChange={this.handleBodyChange}></textarea>
            </div>
            <div className="form-block">
              <label>Description:</label>
              <input name="response_description" type="text"  value={this.state.description} onChange={this.handleDescriptionChange} />
            </div>
            <div className="form-block"><input type="submit" value="Save response" /></div>
          </form>

          <section>
            <h4>Body preview</h4>
            {Object.keys(this.state.bodyParsed).length > 0 && <ReactJson src={this.state.bodyParsed} displayDataTypes={false} onEdit={this.handleJsonEdit} onDelete={this.handleJsonEdit}  onAdd={this.handleJsonEdit} /> }

          </section>
      </div>
    );
  }
}

export default AddResponse;
