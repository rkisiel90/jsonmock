import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { responsesDbRef } from '../../../fire';


class ResponsesList extends Component {
  constructor() {
    super();
    this.state = {
      fetching: true,
      responses: []
    };
  }
  componentDidMount() {
    responsesDbRef.on('child_added', snap => {
      this.addResponseToState({
        key: snap.key,
        body: snap.val().body,
        description: snap.val().description
      });
    });
  }

  componentWillUnmount() {
    responsesDbRef.off();
  }

  addResponseToState(response) {
    this.setState({
      fetching: false,
      responses: [...this.state.responses, response]
    });
  }

  render() {
    return (
      <div className="ResponsesList">
        <h2> ResponsesList: </h2>
        {this.state.fetching ?
          <p>Fetching...</p>
          : <div>
            {this.state.responses.length ?
              <ul>
                {this.state.responses.map((item) =>
                  <li key={item.key}>
                    <Link to={`/response/${item.key}`}>{item.key}</Link>

                  </li>
                )}
              </ul>
              : <strong>We don't have any responses</strong>
            }
          </div>
        }

      </div>
    );
  }
}

export default ResponsesList;
