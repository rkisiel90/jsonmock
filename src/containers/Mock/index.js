import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';

import { enhanceResponsesWithUnique, enhanceItemWithUnique } from '../../utils';
import { mocksDbRef } from '../../fire';

import MockEditForm from './EditForm';

class Mock extends Component {
  constructor(){
    super();
    this.state = {
      id: '',
      fetching: true,
      responses: [],
      responsesChanged: false,
      newResponse: '',
      renameMode: false
    };

    this.getMockData = this.getMockData.bind(this);
    this.onSortEnd = this.onSortEnd.bind(this);
    this.handleResponsesSave = this.handleResponsesSave.bind(this);
    this.handleResponseRemove = this.handleResponseRemove.bind(this);
    this.handleStepReset = this.handleStepReset.bind(this);
    this.handleMockDrop = this.handleMockDrop.bind(this);
    this.toggleMockRenameModeHandler = this.toggleMockRenameModeHandler.bind(this);
    this.onNewResponseChange = this.onNewResponseChange.bind(this);
    this.addNewResponseToResponses = this.addNewResponseToResponses.bind(this);
  }

  componentDidMount(){
    this.getMockData();
  }

  getMockData() {
    mocksDbRef.child(this.props.match.params.id).once('value').then(snap => {
      const snapValue = snap.val();

      this.setState({
        id: this.props.match.params.id,
        fetching: false,
        step: snapValue.step,
        responses: enhanceResponsesWithUnique(snapValue.responses)
      });
    });
  }


  onSortEnd({oldIndex, newIndex}) {
    this.setState({
      responses: arrayMove(this.state.responses, oldIndex, newIndex),
      responsesChanged: true
    });
  }

  onNewResponseChange(event) {

    this.setState({
      newResponse: event.target.value
    });
  }

  addNewResponseToResponses(e) {
    e.preventDefault();

    this.setState({
      responses: [...this.state.responses, enhanceItemWithUnique(this.state.newResponse)],
      responsesChanged: true,
      newResponse: ''
    })
  }

  handleResponsesSave() {
    const responsesToSave = this.state.responses.map(resp => resp.id);

    mocksDbRef.child(this.props.match.params.id).update({
      responses: responsesToSave
    });

    this.setState({
      responsesChanged: false
    });
  }

  handleResponseRemove(responseUnique) {
    const responsesWithoutDeleted = this.state.responses.filter(resp => resp.unique !== responseUnique);

    this.setState({
      responses: responsesWithoutDeleted,
      responsesChanged: true
    });
  }

  handleStepReset(responseId) {
    this.setState({
      step: 0
    });

    mocksDbRef.child(this.props.match.params.id).update({
      step: 0
    });
  }

  handleMockDrop() {
    var isSure = window.confirm('Are you sure?');

    if(isSure) {
      mocksDbRef.child(this.props.match.params.id).remove();
      this.props.history.push(`/mocks/browse`);
    }
  }

  toggleMockRenameModeHandler() {
    this.setState({
      renameMode: !this.state.renameMode
    });
  }


  render() {
    return (
      <div className="Mock">
        {this.state.fetching ?
          <p>Fetching...</p>
          : <div>
            {this.state.responses.length ?
              <div>
                <div>
                  <button disabled={this.state.renameMode} onClick={this.toggleMockRenameModeHandler}>Edit</button>
                  <button onClick={this.handleMockDrop}>Drop</button>
                </div>
                <h2>
                  Mock {this.props.match.params.id}
                </h2>



                { this.state.renameMode &&
                  <MockEditForm
                    mockId={this.state.id}
                    toggleMockRenameModeHandler={this.toggleMockRenameModeHandler}
                    historyRef={this.props.history}
                    getMockData={this.getMockData}
                  />
                }

                <p> Current step: {this.state.step} {this.state.step > 0 && <button onClick={this.handleStepReset}>Reset</button>}</p>
                <h4>Responses: {this.state.responsesChanged && <button onClick={this.handleResponsesSave}>Save</button> }</h4>
                <SortableListOfResponses items={this.state.responses} onSortEnd={this.onSortEnd} pressDelay={200} removeResponseHandler={this.handleResponseRemove}/>
                <hr />

                <form>
                  <div>
                    <label>Add new response (by id) to mock: </label>
                    <input type="text" placeholder="Id of response" onChange={this.onNewResponseChange} value={this.state.newResponse} />
                    <button onClick={this.addNewResponseToResponses}>Add</button>
                  </div>
                </form>
              </div>
              : <h2> Mock {this.props.match.params.id} doesn't exist. </h2>
            }
          </div>
        }
      </div>
    );
  }
}

const SortableItem = SortableElement(({value, removeResponseHandler}) =>
  <li><Link to={`/response/${value.id}`}>{value.id}</Link> <span onClick={() => removeResponseHandler(value.unique)}> remove </span></li>
);

const SortableListOfResponses = SortableContainer(({items, removeResponseHandler}) => {
  return (
    <ul>
      {items.map((value, index) => (
        <SortableItem key={value.unique} index={index} value={value} removeResponseHandler={removeResponseHandler}/>
      ))}
    </ul>
  );
});


export default Mock;
