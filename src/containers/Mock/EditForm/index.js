import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { mocksDbRef } from '../../../fire';


class MockEditForm extends Component {
  constructor(props) {
    super(props);

    // using props in initalState might be perceived as antiPattern but here it's only seed data for the component’s internally-controlled state
    // @see: https://reactjs.org/docs/react-component.html#constructor
    // @see: https://medium.com/@justintulk/react-anti-patterns-props-in-initial-state-28687846cc2e
    this.state = {
      initialId: props.mockId,
      id: props.mockId,
      dropExistingMock: true
    };

    this.handleMockIdChange = this.handleMockIdChange.bind(this);
    this.handleMockEditSubmit = this.handleMockEditSubmit.bind(this);
    this.editMock = this.editMock.bind(this);
    this.handleMockDuplicate = this.handleMockDuplicate.bind(this);
  }

  componentDidMount() {
    this.idInput.focus();
    this.idInput.select();
  }

  handleMockIdChange(e) {
    this.setState({
      id: e.target.value
    });
  }

  async handleMockDuplicate() {

    // mark that we should drop current Mock while editing
    this.setState({
      dropExistingMock: false
    });

    await this.editMock();

    // reset to default
    this.setState({
      dropExistingMock: true
    });
  }

  handleMockEditSubmit(e) {
    e.preventDefault();

    this.editMock();
  }

  async editMock(){
    try {
      const alreadyExist = await mocksDbRef.child(this.state.id).once('value');

      if(alreadyExist.val() === null) {
        // get current shape of mock
        const mockShape = await mocksDbRef.child(this.state.initialId).once('value')

        // save current mock shape on different id
        await mocksDbRef.child(this.state.id).set(mockShape.val());

        if(this.state.dropExistingMock) {
          await mocksDbRef.child(this.state.initialId).remove();
        }

        // redirect to new
        this.props.historyRef.push(`/mock/${this.state.id}`);

        // close edit mode
        this.props.toggleMockRenameModeHandler();

        // close edit mode
        this.props.getMockData();
      } else {
        alert('This mock id already exists!');
      }


    } catch(error) {
      console.error(error);
    }
  }

  render() {
    return (
      <form onSubmit={this.handleMockEditSubmit}>

        <input type="text" value={this.state.id} onChange={this.handleMockIdChange} ref={(input) => { this.idInput = input; }} />

        <button type="submit" disabled={this.state.initialId === this.state.id || !this.state.id}> Rename </button>
        <button type="button" onClick={this.handleMockDuplicate} disabled={this.state.initialId === this.state.id || !this.state.id}> Duplicate </button>
        <button type="button" onClick={this.props.toggleMockRenameModeHandler}> Cancel </button>
      </form>
    );
  }
}

MockEditForm.propTypes = {
  mockId: PropTypes.string.isRequired,
  toggleMockRenameModeHandler: PropTypes.func.isRequired,
  historyRef: PropTypes.object.isRequired,
  getMockData: PropTypes.func.isRequired
};

export default MockEditForm;