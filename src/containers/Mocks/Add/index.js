import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { SortableContainer, SortableElement, arrayMove } from 'react-sortable-hoc';

import { mocksDbRef } from '../../../fire';
import { enhanceItemWithUnique } from '../../../utils';

class AddMock extends Component {
  constructor(){

    super();
    this.state = {
      id: '',
      responses: [],
      response: {
        value: ''
      }
    };

    this.newMock = {};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleResponseRemove = this.handleResponseRemove.bind(this);
    this.addResponseToResponses = this.addResponseToResponses.bind(this);
    this.onResponseChange = this.onResponseChange.bind(this);
    this.onIdChange = this.onIdChange.bind(this);
    this.setDefaultState = this.setDefaultState.bind(this);
    this.onSortEnd = this.onSortEnd.bind(this);
  }

  onSortEnd({oldIndex, newIndex}) {
    this.setState({
      responses: arrayMove(this.state.responses, oldIndex, newIndex),
    });
  };

  componentDidMount() {
    // get new id reference automatically
    this.newMock = mocksDbRef.push();
    this.setState({id: this.newMock.key})
  }

  onResponseChange(event) {
    this.setState({ response: { value: event.target.value }});
  }

  onIdChange(event) {
    this.setState({id: event.target.value});
  }

  addResponseToResponses(e) {
    e.preventDefault();

    if (this.state.response.value) {
      this.setState({
        response: {value: ''},
        responses: [...this.state.responses, enhanceItemWithUnique(this.state.response.value)]
      });
    }
  }

  handleSubmit(e) {

    e.preventDefault();

    if(this.state.responses.length < 1) {
      alert('You should add at least one response to the mock!');
      return;
    }

    // should we use auto-generated reference or should we create our own?
    const newEntryRef = (this.newMock.key === this.state.id) ? this.newMock : mocksDbRef.child(this.state.id);

    newEntryRef.set({
      responses: this.state.responses.map(resp => resp.id),
      step: 0
    });

    this.setDefaultState();
  }

  setDefaultState() {
    this.newMock = mocksDbRef.push();

    this.setState({
      id: this.newMock.key,
      responses: [],
      response: {
        value: ''
      }
    });
  }

  handleBodyChange(event) {

    try {
      this.setState({bodyParsed: JSON.parse(event.target.value)})
    } catch(e) {
      this.setState({bodyParsed: {}})
    }

    this.setState({body: event.target.value});
  }

  handleDescriptionChange(event) {

    this.setState({description: event.target.value});
  }

  handleResponseRemove(repsonseId) {

    const responsesWithoutDeleted = this.state.responses.filter(resp => resp.id !== repsonseId);

    this.setState({
      responses: responsesWithoutDeleted
    });
  }

  render() {
    return (
      <div className="AddMockWrapper">
        <h2> Add new mock: </h2>

          <form onSubmit={this.handleSubmit}>
            <div>
              <div className="form-block">
                <label>Id: </label>
                <input type="text" onChange={this.onIdChange} value={this.state.id} />
              </div>

              <div className="form-block">
                <label>Add new response (by id) to mock: </label>
                <input type="text" placeholder="Id of response" onChange={this.onResponseChange} value={this.state.response.value} />
                <button onClick={this.addResponseToResponses}>Add</button>
              </div>

              <p>Responses (you can change the order - dnd): </p>
              {this.state.responses.length === 0 && <span> -- empty -- </span>}

              <SortableListOfResponses items={this.state.responses} onSortEnd={this.onSortEnd} pressDelay={200} removeResponseHandler={this.handleResponseRemove}/>
            </div>
            <div className="form-block">
              <input type="submit" value="Save mock" />
            </div>
          </form>


      </div>

    );
  }
}


const SortableItem = SortableElement(({value, removeResponseHandler}) =>
  <li><Link to={`/response/${value.id}`}>{value.id}</Link> <span onClick={() => removeResponseHandler(value.id)}> remove </span></li>
);

const SortableListOfResponses = SortableContainer(({items, removeResponseHandler}) => {
  return (
    <ul>
      {items.map((value, index) => (
        <SortableItem key={`item-${index}`} index={index} value={value} removeResponseHandler={removeResponseHandler}/>
      ))}
    </ul>
  );
});


export default AddMock;
