import React, { Component } from 'react';
import MocksList from '../List';


class BrowseMocks extends Component {
  constructor(){
    super();

    this.state = {
    };
  }


  render() {
    return (
      <div className="BrowseMocksWrapper">
        <h2>Browse mocks: </h2>

        <MocksList />
      </div>
    );
  }
}

export default BrowseMocks;
