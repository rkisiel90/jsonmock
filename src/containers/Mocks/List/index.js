import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import shortid from 'shortid';

import { enhanceResponsesWithUnique } from '../../../utils';
import { mocksDbRef } from '../../../fire';

class MocksList extends Component {
  constructor(){
    super();
    this.state = {
      fetching: true,
      mocks: []
    };
  }

  componentDidMount(){
    mocksDbRef.on('child_added', snap => {
      this.addMockToState({
        key: snap.key,
        unique: shortid.generate(),
        responses: enhanceResponsesWithUnique(snap.val().responses)
      });
    });
  }

  componentWillUnmount(){
    mocksDbRef.off();
  }

  addMockToState(mock) {
    this.setState({
      fetching: false,
      mocks: [...this.state.mocks, mock]
    });
  }

  render() {
    return (
      <div className="MocksList">
        <h2> MocksList: </h2>

        {this.state.fetching ?
          <p>Fetching...</p>
          : <div>

            {this.state.mocks.length ?
              <ul>
                {this.state.mocks.map((mock) =>
                  <li key={mock.key}>
                    <h4><Link to={`/mock/${mock.key}`}>{mock.key}</Link></h4>
                    {mock.responses && mock.responses.length &&
                      <span>
                        <p>This mock consists of following responses:</p>
                      <ul>
                        {mock.responses.map((response) =>
                          <li key={response.unique}><Link to={`/response/${response.id}`}>{response.id}</Link></li>
                        )}
                      </ul>
                      </span>
                    }

                  </li>
                )}
              </ul>
              : <strong>We don't have any responses</strong>
            }
          </div>
        }
      </div>
    );
  }
}

export default MocksList;
