import React, { Component } from 'react';
import ReactJson from 'react-json-view';

import { responsesDbRef } from '../../fire';

class Response extends Component {
  constructor(){
    super();
    this.state = {
      id: ''
    };
  }
  componentDidMount(){

    responsesDbRef.child(this.props.match.params.id).once('value').then(snap => {
      this.setState({
        id: this.props.match.params.id,
        ...snap.val()
      })
    });
  }

  render() {
    return (
      <div className="Response">
        <h2> Response {this.props.match.params.id}: </h2>
        {this.state.description &&
          <p>Description: {this.state.description}</p>
        }
        {this.state.body &&
          <div>
            <p>Body:</p>

            <ReactJson src={this.state.body} displayDataTypes={false} />
          </div>
        }
      </div>
    );
  }
}

export default Response;
